# Commit IDs for the (unversioned) redis-doc repository
# https://fedoraproject.org/wiki/Packaging:SourceURL "Commit Revision"
%global doc_commit 3541d0e20cc4bb7873bdbf51a7717757b806577f
%global short_doc_commit %(c=%{doc_commit}; echo ${c:0:7})
 
%global macrosdir %(d=%{_rpmconfigdir}/macros.d; [ -d $d ] || d=%{_sysconfdir}/rpm; echo $d)

Name:              redis
Version:           7.2.7
Release:           1
Summary:           A persistent key-value database
# redis, hiredis: BSD-3-Clause
# hdrhistogram, jemalloc, lzf, linenoise: BSD-2-Clause
# lua: MIT
# fpconv: BSL-1.0
License:           BSD-3-Clause AND BSD-2-Clause AND MIT AND BSL-1.0 AND CC-BY-SA-4.0
URL:               https://redis.io
Source0:           http://download.redis.io/releases/%{name}-%{version}.tar.gz
Source1:           %{name}.logrotate
Source2:           %{name}-sentinel.service
Source3:           %{name}.service
Source4:           %{name}-limit-systemd
Source5:           macros.%{name}
Source6:           https://github.com/%{name}/%{name}-doc/archive/%{doc_commit}/%{name}-doc-%{short_doc_commit}.tar.gz
# https://github.com/redis/redis/pull/3491 - man pages
Patch0001:         0001-1st-man-pageis-for-redis-cli-redis-benchmark-redis-c.patch

BuildRequires:     systemd
BuildRequires:     systemd-devel
BuildRequires:     gcc
BuildRequires:     make
BuildRequires:     openssl-devel
Requires:          /bin/awk
Requires:          logrotate
Requires(pre):     shadow-utils
Requires(post):    systemd
Requires(preun):   systemd
Requires(postun):  systemd

%global redis_modules_abi 1
%global redis_modules_dir %{_libdir}/%{name}/modules
Provides:          redis(modules_abi)%{?_isa} = %{redis_modules_abi}

%description
Redis is an advanced key-value store. It is often referred to as a dattructure server since keys can contain strings, hashes
,lists, sets anorted sets.

%prep
%setup -q -n %{name}-%{version} -b 6
mv ../%{name}-doc-%{doc_commit} doc
%patch -P0001 -p1

mv deps/lua/COPYRIGHT             COPYRIGHT-lua
mv deps/jemalloc/COPYING          COPYING-jemalloc
mv deps/hiredis/COPYING           COPYING-hiredis
mv deps/hdr_histogram/LICENSE.txt LICENSE-hdrhistogram
mv deps/hdr_histogram/COPYING.txt COPYING-hdrhistogram
mv deps/fpconv/LICENSE.txt        LICENSE-fpconv

# Configuration file changes
sed -i -e 's|^logfile .*$|logfile /var/log/redis/redis.log|g' redis.conf
sed -i -e 's|^logfile .*$|logfile /var/log/redis/sentinel.log|g' sentinel.conf
sed -i -e 's|^dir .*$|dir /var/lib/redis|g' redis.conf

# See https://bugzilla.redhat.com/2240293
# See https://src.fedoraproject.org/rpms/jemalloc/blob/rawhide/f/jemalloc.spec#_34
%ifarch %ix86 %arm x86_64 s390x
sed -e 's/--with-lg-quantum/--with-lg-page=12 --with-lg-quantum/' -i deps/Makefile
%endif
%ifarch ppc64 ppc64le aarch64
sed -e 's/--with-lg-quantum/--with-lg-page=16 --with-lg-quantum/' -i deps/Makefile
%endif

# Module API version safety check
api=`sed -n -e 's/#define REDISMODULE_APIVER_[0-9][0-9]* //p' src/redismodule.h`
if test "$api" != "%{redis_modules_abi}"; then
   : Error: Upstream API version is now ${api}, expecting %%{redis_modules_abi}.
   : Update the redis_modules_abi macro, the rpmmacros file, and rebuild.
   exit 1
fi

%global make_flags	DEBUG="" V="echo" LDFLAGS="%{?__global_ldflags}" CFLAGS+="%{optflags} -fPIC" INSTALL="install -p" PREFIX=%{buildroot}%{_prefix} BUILD_WITH_SYSTEMD=yes BUILD_TLS=yes

%build
%make_build %{make_flags} all

%install
make %{make_flags} install

# Filesystem.
install -d %{buildroot}%{_sharedstatedir}/%{name}
install -d %{buildroot}%{_localstatedir}/log/%{name}
install -d %{buildroot}%{_localstatedir}/run/%{name}
install -d %{buildroot}%{_libdir}/%{name}/modules

# Install logrotate file.
install -pDm644 %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}

# Install systemd unit files.
mkdir -p %{buildroot}%{_unitdir}
install -pm644 %{SOURCE2} %{buildroot}%{_unitdir}
install -pm644 %{SOURCE3} %{buildroot}%{_unitdir}

# Install configuration files.
install -pDm640 %{name}.conf  %{buildroot}%{_sysconfdir}/%{name}.conf
install -pDm640 sentinel.conf %{buildroot}%{_sysconfdir}/%{name}-sentinel.conf

# Install systemd limit files (requires systemd >= 204)
install -p -D -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/systemd/system/%{name}.service.d/limit.conf
install -p -D -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/systemd/system/%{name}-sentinel.service.d/limit.conf

# Fix non-standard-executable-perm error.
chmod 755 %{buildroot}%{_bindir}/%{name}-*

# Install redis module header
install -pDm644 src/%{name}module.h %{buildroot}%{_includedir}/%{name}module.h

# Install man pages
man=$(dirname %{buildroot}%{_mandir})
for page in man/man?/*; do
    install -Dpm644 $page $man/$page
done
ln -s redis-server.1 %{buildroot}%{_mandir}/man1/redis-sentinel.1
ln -s redis.conf.5   %{buildroot}%{_mandir}/man5/redis-sentinel.conf.5

# Install documentation and html pages
doc=$(echo %{buildroot}/%{_docdir}/%{name})
for page in 00-RELEASENOTES BUGS MANIFESTO *.md; do
    install -Dpm644 $page $doc/$page
done
for page in $(find doc -name \*.md | sed -e 's|.md$||g'); do
    base=$(echo $page | sed -e 's|doc/||g')
    install -Dpm644 $page.md $doc/$base.md
done

# Install rpm macros for redis modules
mkdir -p %{buildroot}%{macrosdir}
install -pDm644 %{SOURCE5} %{buildroot}%{macrosdir}/macros.%{name}

%pre
getent group %{name} &> /dev/null || \
groupadd -r %{name} &> /dev/null
getent passwd %{name} &> /dev/null || \
useradd -r -g %{name} -d %{_sharedstatedir}/%{name} -s /sbin/nologin \
-c 'Redis Database Server' %{name} &> /dev/null
exit 0

%post
%systemd_post %{name}.service
%systemd_post %{name}-sentinel.service

%preun
%systemd_preun %{name}.service
%systemd_preun %{name}-sentinel.service

%postun
%systemd_postun_with_restart %{name}.service
%systemd_postun_with_restart %{name}-sentinel.service


%files
%license COPYING
%license COPYRIGHT-lua
%license COPYING-jemalloc
%license COPYING-hiredis
%license LICENSE-hdrhistogram
%license COPYING-hdrhistogram
%license LICENSE-fpconv
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%attr(0640, redis, root) %config(noreplace) %{_sysconfdir}/%{name}.conf
%attr(0640, redis, root) %config(noreplace) %{_sysconfdir}/%{name}-sentinel.conf
%dir %attr(0750, redis, redis) %{_libdir}/%{name}
%dir %attr(0750, redis, redis) %{_libdir}/%{name}/modules
%dir %attr(0750, redis, redis) %{_sharedstatedir}/%{name}
%dir %attr(0750, redis, redis) %{_localstatedir}/log/%{name}
%{_bindir}/%{name}-*
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}-sentinel.service
%dir %{_sysconfdir}/systemd/system/%{name}.service.d
%config(noreplace) %{_sysconfdir}/systemd/system/%{name}.service.d/limit.conf
%dir %{_sysconfdir}/systemd/system/%{name}-sentinel.service.d
%config(noreplace) %{_sysconfdir}/systemd/system/%{name}-sentinel.service.d/limit.conf
%{_mandir}/man1/%{name}*
%{_mandir}/man5/%{name}*

%{_includedir}/%{name}module.h
%{macrosdir}/*

# specific for documentation (CC-BY-SA)
%license doc/LICENSE
%docdir %{_docdir}/%{name}
%{_docdir}/%{name}

%changelog
* Wed Feb 12 2025 wangkai <13474090681@163.com> - 7.2.7-1
- Update to 7.2.7
- Fix CVE-2021-32626 CVE-2021-32627 CVE-2021-32762 CVE-2021-32675
  CVE-2021-41099 CVE-2021-32628 CVE-2021-32687 CVE-2022-24735
  CVE-2022-24736 CVE-2022-3647 CVE-2022-3734 CVE-2022-35977
  CVE-2023-25155 CVE-2024-46981

* Wed Oct 09 2024 yaoxin <yao_xin001@hoperun.com> - 4.0.14-7
- Fix CVE-2023-45145,CVE-2024-31228 and CVE-2024-31449

* Mon Jul 31 2023 wangkai <13474090681@163.com> - 4.0.14-6
- Fix CVE-2022-24834

* Mon May 15 2023 yaoxin <yao_xin001@hoperun.com> - 4.0.14-5
- Fix CVE-2023-28856

* Mon Mar 27 2023 wushaozheng<wushaozheng@ncti-gba.cn> - 4.0.14-4
- Fix CVE-2022-36021

* Tue Nov 15 2022 huajingyun<huajingyun@loongson.cn> - 4.0.14-3
- Update config.guess and config.sub for loongarch

* Tue May 10 2022 chenchen <chen_aka_jan@163.com> - 4.0.14-2
- License compliance rectification

* Wed Dec 29 2021 zhangjiapeng <zhangjiapeng9@huawei.com> - 4.0.14-1
- Update to 4.0.14

* Sat Oct 09 2021 yaoxin <yaoxin30@huawei.com> - 4.0.11-18
- Fix CVE-2021-32672

* Tue Jun 08 2021 wangyue <wangyue92@huawei.com> - 4.0.11-17
- Fix CVE-2021-29478

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 4.0.11-16
- Completing build dependencies to fix gcc compiler missing error

* Wed Apr 07 2021 wangyue <wangyue92@huawei.com> - 4.0.11-15
- Fix CVE-2021-3470

* Wed Mar 24 2021 sunguoshuai <sunguoshuai@huawei.com> - 4.0.11-14
- change patch file in order src.rpm is same in aarch64 and x86_64
 
* Thu Mar 11 2021 wangxiao <wangxiao65@huawei.com> - 4.0.11-13
- Fix CVE-2021-21309

* Wed Nov 11 2020 yangyanchao <yangyanchao6@huawei.com> - 4.0.11-12
- enable dependency package jemalloc of redis to support riscv 

* Wed Sep 30 2020 Jiapeng Zhang <zhangjiapeng9@huawei.com> - 4.0.11-11
- Modify aarch64 architecture jemalloc page size from 4k to 64k

* Mon Sep 21 2020 wangyue <wangyue92@huawei.com> - 4.0.11-10
- fix CVE-2019-10193

* Mon Jul 20 2020 wangxiao <wangxiao654@huawei.com> - 4.0.11-9
- fix CVE-2020-14147

* Fri Jun 19 2020 Captain Wei <captain.a.wei@gmail.com> - 4.0.11-8
- Add some dependency package in building and running phase

* Fri Jun 12 2020 panchenbo <panchenbo@uniontech.com> - 4.0.11-7
- Type:bugfix
- ID: NA
- SUG: restart
- DESC: Resolve service startup failure whthout no %pre

* Mon Jun 01 2020 huanghaitao <huanghaitao8@huawei.com> - 4.0.11-6
- Resolve service startup failure
 
* Tue Mar 17 2020 wangye <wangye54@huawei.com> - 4.0.11-5
- CVE name fix

* Tue Mar 17 2020 wangye <wangye54@huawei.com> - 4.0.11-4
- CVE fix

* Wed Jan 15 2020 zhujunhao <zhujunhao5@huawei.com> - 4.0.11-3
- Modify redis service

* Wed Jan 08 2020 lijin Yang <yanglijin@openeuler.org> - 4.0.11-2
- Package init

